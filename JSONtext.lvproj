﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Config Files" Type="Folder">
			<Item Name="Config File.lvclass" Type="LVClass" URL="../Config Files/Config File.lvclass"/>
			<Item Name="Config Tester 2.vi" Type="VI" URL="../Tests/Config Tester 2.vi"/>
			<Item Name="Config Tester App 1.vi" Type="VI" URL="../Tests/Config Tester App 1.vi"/>
			<Item Name="Config Tester App 2.vi" Type="VI" URL="../Tests/Config Tester App 2.vi"/>
			<Item Name="Config Tester.vi" Type="VI" URL="../Tests/Config Tester.vi"/>
		</Item>
		<Item Name="Tests" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="JSONtext All Tests.vi" Type="VI" URL="../Tests/JSONtext All Tests.vi"/>
			<Item Name="Test Section Cluster Tools.vi" Type="VI" URL="../Tests/Test Section Cluster Tools.vi"/>
			<Item Name="Test ToFrom JSONpath Array.vi" Type="VI" URL="../Tests/Test ToFrom JSONpath Array.vi"/>
			<Item Name="JSONtext Multi-step Filter.vi" Type="VI" URL="../Tests/JSONtext Multi-step Filter.vi"/>
			<Item Name="Test Case Error or No Error.vi" Type="VI" URL="../Tests/Test Case Error or No Error.vi"/>
			<Item Name="Test all Data Types in Variant.vi" Type="VI" URL="../Tests/Test all Data Types in Variant.vi"/>
			<Item Name="Delete Insert Test.vi" Type="VI" URL="../Tests/Delete Insert Test.vi"/>
			<Item Name="Timed Test.vi" Type="VI" URL="../Tests/Timed Test.vi"/>
			<Item Name="Flattening of SGL arrays.vi" Type="VI" URL="../Exclude from VIPM/Flattening of SGL arrays.vi"/>
			<Item Name="Example of Arrays to JSON.vi" Type="VI" URL="../Tests/Example of Arrays to JSON.vi"/>
			<Item Name="Get and Set Test.vi" Type="VI" URL="../Tests/Get and Set Test.vi"/>
			<Item Name="Find Multiple by Path Test.vi" Type="VI" URL="../Tests/Find Multiple by Path Test.vi"/>
			<Item Name="Find by Path Test.vi" Type="VI" URL="../Tests/Find by Path Test.vi"/>
			<Item Name="Find by Path Test 2.vi" Type="VI" URL="../Tests/Find by Path Test 2.vi"/>
			<Item Name="INI to JSON Test.vi" Type="VI" URL="../Tests/INI to JSON Test.vi"/>
			<Item Name="Type Specialization.vim" Type="VI" URL="../Exclude from VIPM/Type Specialization.vim"/>
			<Item Name="Test Empty Invalid Null.vi" Type="VI" URL="../Tests/Test Empty Invalid Null.vi"/>
			<Item Name="Diff Test.vi" Type="VI" URL="../Tests/Diff Test.vi"/>
			<Item Name="Correct Error codes.vi" Type="VI" URL="../Tests/Correct Error codes.vi"/>
			<Item Name="Test Returns Default.vi" Type="VI" URL="../Tests/Test Returns Default.vi"/>
			<Item Name="Test DAQmx tags.vi" Type="VI" URL="../support/Test DAQmx tags.vi"/>
			<Item Name="Set Item Test.vi" Type="VI" URL="../Tests/Set Item Test.vi"/>
			<Item Name="Get Item Test.vi" Type="VI" URL="../Tests/Get Item Test.vi"/>
			<Item Name="JOBJ.vi" Type="VI" URL="../Tests/JOBJ.vi"/>
			<Item Name="Not Equivalent Test.vi" Type="VI" URL="../Tests/Not Equivalent Test.vi"/>
			<Item Name="Not Equivalent Test 2.vi" Type="VI" URL="../Tests/Not Equivalent Test 2.vi"/>
			<Item Name="Test Flatten SubClusters.vi" Type="VI" URL="../Tests/Test Flatten SubClusters.vi"/>
			<Item Name="Experimental Test.vi" Type="VI" URL="../Tests/Experimental Test.vi"/>
			<Item Name="Null treatment.vi" Type="VI" URL="../Tests/Null treatment.vi"/>
			<Item Name="Delete with Comments Test.vi" Type="VI" URL="../Tests/Delete with Comments Test.vi"/>
			<Item Name="Test Merge JSON Objects.vi" Type="VI" URL="../Tests/Test Merge JSON Objects.vi"/>
			<Item Name="Test Flatten to Path.vi" Type="VI" URL="../Tests/Test Flatten to Path.vi"/>
			<Item Name="Test single values are equivelent to 1-element arrays.vi" Type="VI" URL="../Tests/Test single values are equivelent to 1-element arrays.vi"/>
			<Item Name="Test Custom LVClass Serialization.vi" Type="VI" URL="../Tests/Test Custom LVClass Serialization.vi"/>
			<Item Name="Base64url.vi" Type="VI" URL="../Tests/Base64url.vi"/>
			<Item Name="Insertion Tests.vi" Type="VI" URL="../Tests/Insertion Tests.vi"/>
			<Item Name="DEV scaler to LV Array Issue 67.vi" Type="VI" URL="../Tests/DEV scaler to LV Array Issue 67.vi"/>
			<Item Name="Test Comments.vi" Type="VI" URL="../Tests/Test Comments.vi"/>
			<Item Name="Strings.vi" Type="VI" URL="../support/Strings.vi"/>
			<Item Name="Permisive Commas.vi" Type="VI" URL="../Tests/Permisive Commas.vi"/>
			<Item Name="Pretty Print Speed Test.vi" Type="VI" URL="../Tests/Pretty Print Speed Test.vi"/>
			<Item Name="Pretty Print Methods Test.vi" Type="VI" URL="../Tests/Pretty Print Methods Test.vi"/>
			<Item Name="Integer and Enum Type match on Flattening.vi" Type="VI" URL="../Tests/Integer and Enum Type match on Flattening.vi"/>
			<Item Name="Test Parallel Config File Writes.vi" Type="VI" URL="../Tests/Test Parallel Config File Writes.vi"/>
		</Item>
		<Item Name="Examples" Type="Folder" URL="../Examples">
			<Property Name="NI.DISK" Type="Bool">true</Property>
			<Property Name="NI.SortType" Type="Int">0</Property>
		</Item>
		<Item Name="Probes" Type="Folder">
			<Item Name="JSONtext Pretty Print.vi" Type="VI" URL="../Probes/JSONtext Pretty Print.vi"/>
			<Item Name="JSONtext Pretty Print (Colour).vi" Type="VI" URL="../Probes/JSONtext Pretty Print (Colour).vi"/>
		</Item>
		<Item Name="Documentation Tools" Type="Folder">
			<Item Name="Control Descriptions Updater.vi" Type="VI" URL="../Documentation Tools/Control Descriptions Updater.vi"/>
			<Item Name="Error Cluster Descriptions Updater.vi" Type="VI" URL="../Documentation Tools/Error Cluster Descriptions Updater.vi"/>
			<Item Name="API.vi" Type="VI" URL="../Documentation Tools/API.vi"/>
			<Item Name="Print to HTML MOD.vi" Type="VI" URL="../Documentation Tools/Print to HTML MOD.vi"/>
		</Item>
		<Item Name="JSONtext Help" Type="Folder" URL="../JSONtext Help">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Help Menu item" Type="Folder">
			<Item Name="JSONtext Help.vi" Type="VI" URL="../JSONtext Help.vi"/>
			<Item Name="JSONtext Help.txt" Type="Document" URL="../JSONtext Help.txt"/>
		</Item>
		<Item Name="NI Palette" Type="Folder" URL="../NI Palette">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="LVClass Serializer" Type="Folder">
			<Item Name="JSONtext LVClass Serializer.lvclass" Type="LVClass" URL="../LVClass Serializer/JSONtext LVClass Serializer.lvclass"/>
		</Item>
		<Item Name="Base64" Type="Folder">
			<Item Name="Base64 Support.lvlib" Type="Library" URL="../Base64/Base64 Support.lvlib"/>
		</Item>
		<Item Name="JSONtext.lvlib" Type="Library" URL="../JSONtext.lvlib"/>
		<Item Name="JSONtext.vipb" Type="Document" URL="../JSONtext.vipb"/>
		<Item Name="Develop compact LVObject.vi" Type="VI" URL="../Exclude from VIPM/Develop compact LVObject.vi"/>
		<Item Name="JSONtext Tree VI.vi" Type="VI" URL="../Exclude from VIPM/JSONtext Tree VI.vi"/>
		<Item Name="license JSONtext.txt" Type="Document" URL="../license JSONtext.txt"/>
		<Item Name="UTF8 issues tests.vi" Type="VI" URL="../Tests/UTF8 issues tests.vi"/>
		<Item Name="JSONtext-errors.txt" Type="Document" URL="../JSONtext-errors.txt"/>
		<Item Name="TEST Variant to Flattened String (special).vi" Type="VI" URL="../Exclude from VIPM/Test/TEST Variant to Flattened String (special).vi"/>
		<Item Name="test Variant.vi" Type="VI" URL="../Exclude from VIPM/Test/test Variant.vi"/>
		<Item Name="Untitled 2.vi" Type="VI" URL="../Untitled 2.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="SQLite.lvlib" Type="Library" URL="/&lt;vilib&gt;/drjdpowell/SQLite Library/SQLite.lvlib"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="Command Line String To Path.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Command Line String To Path.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="UNIXPathStringToPath.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/UNIXPathStringToPath.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="JDP Utility.lvlib" Type="Library" URL="/&lt;vilib&gt;/JDP Science/JDP Science Common Utilities/JDP Utility.lvlib"/>
				<Item Name="Get File System Separator.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysinfo.llb/Get File System Separator.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Open URL in Default Browser core.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser core.vi"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Open URL in Default Browser (path).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (path).vi"/>
				<Item Name="Open URL in Default Browser (string).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (string).vi"/>
				<Item Name="Open URL in Default Browser.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Caraya.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/Caraya/Caraya.lvlib"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Parse State Queue__jki_lib_state_machine.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/State Machine/_JKI_lib_State_Machine.llb/Parse State Queue__jki_lib_state_machine.vi"/>
				<Item Name="Add State(s) to Queue__jki_lib_state_machine.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/State Machine/_JKI_lib_State_Machine.llb/Add State(s) to Queue__jki_lib_state_machine.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Write Palette.vi" Type="VI" URL="/&lt;vilib&gt;/Palette API/Write Palette.vi"/>
				<Item Name="Palette Menu.lvlib" Type="Library" URL="/&lt;vilib&gt;/Palette API/Palette Menu/Palette Menu.lvlib"/>
				<Item Name="Picture to Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Picture to Pixmap.vi"/>
				<Item Name="Read Palette.vi" Type="VI" URL="/&lt;vilib&gt;/Palette API/Read Palette.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Empty Picture" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Empty Picture"/>
				<Item Name="Has LLB Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Has LLB Extension.vi"/>
				<Item Name="Get VI Library File Info.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get VI Library File Info.vi"/>
				<Item Name="Librarian File Info Out.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info Out.ctl"/>
				<Item Name="Librarian.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian.vi"/>
				<Item Name="Librarian File Info In.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info In.ctl"/>
				<Item Name="Librarian File List.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File List.ctl"/>
				<Item Name="TRef TravTarget.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef TravTarget.ctl"/>
				<Item Name="TRef Traverse for References.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse for References.vi"/>
				<Item Name="VI Scripting - Traverse.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/traverseref.llb/VI Scripting - Traverse.lvlib"/>
				<Item Name="TRef Traverse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse.vi"/>
				<Item Name="JDP Timestamp.lvlib" Type="Library" URL="/&lt;vilib&gt;/JDP Science/JDP Science Common Utilities/Timestamp/JDP Timestamp.lvlib"/>
				<Item Name="Caraya Interactive Menu.rtm" Type="Document" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/Caraya/menu/Caraya Interactive Menu.rtm"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="SendMSG.lvlib" Type="Library" URL="/&lt;vilib&gt;/drjdpowell/Messenging/Core Messages/MSG Library/SendMSG.lvlib"/>
				<Item Name="Variant Name_Messenging.vi" Type="VI" URL="/&lt;vilib&gt;/drjdpowell/Messenging/Utility/Variant Name_Messenging.vi"/>
				<Item Name="VariantFlattenExp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/VariantFlattenExp.vi"/>
				<Item Name="Array of Variants to Cluster_Messenging.vi" Type="VI" URL="/&lt;vilib&gt;/drjdpowell/Messenging/Utility/Array of Variants to Cluster_Messenging.vi"/>
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Shuffle 1D Array.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Shuffle 1D Array.vim"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Physical Units__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Physical Units__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BPhysical Units__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BPhysical Units__ogtk.ctl"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Physical Units from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Physical Units from TD__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BType Descriptor__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BType Descriptor__ogtk.ctl"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet TDEnum from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet TDEnum from TD__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BType Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BType Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Element TD from Array TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Element TD from Array TD__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Header from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Header from TD__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BType Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BType Descriptor Header__ogtk.ctl"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BBuild Error Cluster__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BBuild Error Cluster__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFormat Variant Into String__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFormat Variant Into String__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet TDEnum from Data__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Refnum Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Refnum Type Enum from Data__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRefnum Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRefnum Subtype Enum__ogtk.ctl"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Refnum Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Refnum Type Enum from TD__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BStrip Units__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BStrip Units__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BVariant to Header Info__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BVariant to Header Info__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BArray Size(s)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BArray Size(s)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Array Element TDEnum__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Array Element TDEnum__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Strings from Enum__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Strings from Enum TD__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet PString__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet PString__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Waveform Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Waveform Type Enum from Data__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BWaveform Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BWaveform Subtype Enum__ogtk.ctl"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Waveform Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Waveform Type Enum from TD__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BResolve Timestamp Format__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BResolve Timestamp Format__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BTrim Whitespace__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BTrim Whitespace__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BTrim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BTrim Whitespace (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BTrim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BTrim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BArray to Array of VData__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BArray to Array of VData__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReshape Array to 1D VArray__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReshape Array to 1D VArray__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Data Name__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Data Name__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Data Name from TD__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Last PString__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Last PString__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSet Data Name__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSet Data Name__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BGet Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BGet Variant Attributes__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BCluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BCluster to Array of VData__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSplit Cluster TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSplit Cluster TD__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BParse String with TDs__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BParse String with TDs__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFile Exists__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFile Exists__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFile Exists - Scalar__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFile Exists - Scalar__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFile Exists - Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFile Exists - Array__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BRemove Duplicates from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BList Directory Recursive__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BList Directory Recursive__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BList Directory__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BList Directory__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BBuild Path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BBuild Path__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BBuild Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BBuild Path - Traditional__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BBuild Path - File Names Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BBuild Path - File Names Array__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BBuild Path - File Names and Paths Arrays__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BBuild Path - File Names and Paths Arrays__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BBuild Path - Traditional - path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BBuild Path - Traditional - path__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BBuild Path - File Names Array - path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BBuild Path - File Names Array - path__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BBuild Path - File Names and Paths Arrays - path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BBuild Path - File Names and Paths Arrays - path__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch Array__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSearch 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSearch 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from Array__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort Array__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 1D Array (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BSort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BSort 2D Array (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder Array2__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder Array2__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 1D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BReorder 2D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BDelete Elements from 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BFilter 1D Array with Scalar (LVObject)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BStrip Path Extension__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BStrip Path Extension__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BStrip Path Extension - Path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BStrip Path Extension - Path__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BStrip Path Extension - String__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BStrip Path Extension - String__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BStrip Path Extension - 1D Array of Paths__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BStrip Path Extension - 1D Array of Paths__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BStrip Path Extension - 1D Array of Strings__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BStrip Path Extension - 1D Array of Strings__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty Array__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (CDB)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (CXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (CSG)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (DBL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (EXT)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (I8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (I16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (I32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (Path)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (SGL)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (String)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (U8)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (U16)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (U32)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty Array (Variant)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (I64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (U64)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/3494A183FEBB6A11205EA059762AB79BEmpty 2D Array (LVObject)__ogtk.vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="Convert File Extension__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Convert File Extension__ogtk.vi"/>
				<Item Name="Convert File Extension (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Convert File Extension (Path)__ogtk.vi"/>
				<Item Name="Convert File Extension (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Convert File Extension (String)__ogtk.vi"/>
				<Item Name="Strip Path Extension__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path Extension__ogtk.vi"/>
				<Item Name="Strip Path Extension - Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path Extension - Path__ogtk.vi"/>
				<Item Name="Strip Path Extension - String__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path Extension - String__ogtk.vi"/>
				<Item Name="Strip Path Extension - 1D Array of Paths__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path Extension - 1D Array of Paths__ogtk.vi"/>
				<Item Name="Strip Path Extension - 1D Array of Strings__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path Extension - 1D Array of Strings__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
